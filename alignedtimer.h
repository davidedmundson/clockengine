#include <QObject>
#include <QWeakPointer>

class AlignedTimer : public QObject
{
    Q_OBJECT
public:
    AlignedTimer(int interval);
    ~AlignedTimer();

    static std::shared_ptr<AlignedTimer> getMinuteTimer();
    static std::shared_ptr<AlignedTimer> getSecondTimer();

Q_SIGNALS:
    void timeout();

private:
    void initTimer();
    int m_interval = 1;
    int m_timerFd = -1;
};
