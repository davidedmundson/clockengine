#include <QCoreApplication>
#include <QDebug>
#include "clock.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Clock clock1;
    clock1.classBegin();
    clock1.setTrackSeconds(true);
    clock1.componentComplete();
    QObject::connect(&clock1, &Clock::timeChanged, [&clock1]() {
        qDebug() << clock1.now() << QDateTime::currentDateTime();
    });

    return a.exec();
}
