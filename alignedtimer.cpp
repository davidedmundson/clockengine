#include "alignedtimer.h"

#include <sys/timerfd.h>
#include <unistd.h>
#include <fcntl.h>

#include <qsystemdetection.h>
#include <QSocketNotifier>
#include <QDebug>

#ifndef Q_OS_LINUX
#include <QDBusConnection>
#endif

#ifndef TFD_TIMER_ABSTIME
#define TFD_TIMER_ABSTIME 0x1
#endif

#ifndef TFD_TIMER_CANCEL_ON_SET
#define TFD_TIMER_CANCEL_ON_SET 0x2
#endif

std::shared_ptr<AlignedTimer> AlignedTimer::getMinuteTimer()
{
    static std::weak_ptr<AlignedTimer> s_minuteTimer;

    std::shared_ptr<AlignedTimer> timer = s_minuteTimer.lock();
    if (!timer) {
        timer = std::make_shared<AlignedTimer>(60);
        s_minuteTimer = timer;
    }
    return timer;
}

std::shared_ptr<AlignedTimer> AlignedTimer::getSecondTimer()
{
    static std::weak_ptr<AlignedTimer> s_secondTimer;

    std::shared_ptr<AlignedTimer> timer = s_secondTimer.lock();
    if (!timer) {
        timer = std::make_shared<AlignedTimer>(1);
        s_secondTimer = timer;
    }
    return timer;
}

AlignedTimer::AlignedTimer(int interval)
    : m_interval(interval)
{
    m_timerFd = timerfd_create(CLOCK_REALTIME, O_CLOEXEC | O_NONBLOCK);

    auto notifier = new QSocketNotifier(m_timerFd, QSocketNotifier::Read, this);
    connect(notifier, &QSocketNotifier::activated, this, [this](int fd) {
        uint64_t c = 0;
        read(fd, &c, sizeof(c));
        if (c == 0) { // clock skew detected
            initTimer();
        }
        Q_EMIT timeout();
    });
    initTimer();

    // on linux the TFD_TIMER_CANCEL_ON_SET takes care of everthing
#ifndef Q_OS_LINUX
    QDBusConnection dbus = QDBusConnection::sessionBus();
    dbus.connect(QString(), QStringLiteral("/org/kde/kcmshell_clock"),
                 QStringLiteral("org.kde.kcmshell_clock"), QStringLiteral("clockUpdated"), this, SIGNAL(timeout));

    dbus.connect(QStringLiteral("org.kde.Solid.PowerManagement"),
                 QStringLiteral("/org/kde/Solid/PowerManagement/Actions/SuspendSession"),
                 QStringLiteral("org.kde.Solid.PowerManagement.Actions.SuspendSession"),
                 QStringLiteral("resumingFromSuspend"),
                 this,
                 SIGNAL(timeout()));
#endif
}

AlignedTimer::~AlignedTimer()
{
    if (m_timerFd > 0) {
        close(m_timerFd);
    }
}

void AlignedTimer::initTimer()
{
    itimerspec timespec = {0};
    timespec.it_value.tv_sec = m_interval;
    timespec.it_interval.tv_sec = m_interval;

    // these are not exposed in glibc universally hence redeclared


    int flags = TFD_TIMER_ABSTIME;
#ifdef Q_OS_LINUX
    flags |= TFD_TIMER_CANCEL_ON_SET;
#endif

    int err = timerfd_settime(m_timerFd, flags, &timespec, nullptr);
    if (err) {
        qWarning() << "Could not create timer with TFD_TIMER_CANCEL_ON_SET. Clock skews will not be detected. Error:"
                                   << qPrintable(strerror(err));
        return;
    }
}
